var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/login', function(req, res) {
    res.render('login', { title:'login'});
});

router.get('/main', function(req, res) {
    res.render('main', { title:'Vacantes publicadas' });
});

router.get('/register', function(req, res) {
    res.render('register', { title:'Vacantes publicadas' });
});

router.get('/newprofile', function(req, res) {
    res.render('newprofile', { title:'Vacantes publicadas' });
});

router.get('/selectedVacancy', function(req, res) {
    res.render('selectedVacancy', { title:'Vacantes publicadas' });
});

router.get('/search', function(req, res) {
    res.render('search', { title:'Vacantes publicadas' });
});

module.exports = router;
